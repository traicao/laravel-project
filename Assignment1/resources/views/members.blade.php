<!DOCTYPE html>

<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <style type="text/css">
        tbody td>img {
            position: relative;
            max-width: 50px;
            float: left;
            margin-right: 15px;
        }

        tbody td .user-link {
            display: block;
            font-size: 1.25em;
            padding-top: 3px;
            margin-left: 60px;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>
                            <label class="control-label" style="text-align:left">Member directory</label>
                        </th>
                        <th style="text-align:right">
                            <form method="get" action="search">
                                <label class="control-label">Search Members</label>
                                <input name="txtSearch" type="text" placeholder="Member name" value="@isset($search)  {{$search}} @endisset" />

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button id="btnLogin" type="submit" class="btn btn-default btn-sm" style="text-align:right">Go</button>
                            </form>

                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($members as $m)
                    <tr>
                        <td>
                            <img src="images/{{$m->image}}" alt="">
                            <a href="#" class="user-link">{{$m->firstname .' '. $m->surname }}</a>
                            <span class="user-subhead">{{$m->job}}</span>
                        </td>
                        <td style="text-align:right"><button id="btnLogin" type="button" class="btn btn-info btn-sm" style="text-align:right">View</button></td>

                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div align="center">
                {!! $members->render() !!}
            </div>

        </div>
    </div>


</body>

</html>