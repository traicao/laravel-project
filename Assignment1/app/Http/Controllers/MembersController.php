<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;

class MembersController extends Controller
{
    public function getMember(){
        $members=Member::paginate(4);
        return view('members',['members' =>$members]);
    }
}
