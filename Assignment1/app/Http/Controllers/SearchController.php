<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;

class SearchController extends Controller
{
    public function searchMembers(Request $request){
        $search = $request->input('txtSearch');
        $members = Member::where('firstname','LIKE',"%$search%")
                            ->orWhere('region','LIKE',"%$search%")
                            ->orWhere('interests','LIKE',"%$search%")
                             ->orWhere('specialism','LIKE',"%$search%")->paginate(4);
         return view('members', compact('members', 'search'));
    }
}

