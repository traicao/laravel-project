-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 11, 2017 at 07:47 AM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_assignment1`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` text COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `job` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `region` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `interests` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `specialism` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `email`, `password`, `firstname`, `surname`, `job`, `region`, `interests`, `specialism`, `image`) VALUES
(1, 'traicao@gmail.com', '12345abc', 'Trai', 'Cao', 'Developper', 'Quang Nam', 'Soccer', 'IT', 'traicao.png'),
(2, 'namle@gmail.com', 'abcde123', 'Nam', 'Le', 'Developer', 'Hue', 'Read IT News', 'IT', 'namle.png'),
(3, 'billgate@gmail.com', '123asdf', 'Bill', 'Gate', 'Manager', 'USA', 'Develop', 'Computer Science', 'billgate.png'),
(4, 'dongnhi@gmail.com', '12342536', 'Nhi', 'Dong', 'Singer', 'VietNam', 'shoppping', 'Sing', 'dongnhi.png'),
(5, 'hoailinh@gmail.com', '12536845', 'Linh', 'Hoai', 'Acter', 'VietNam', 'Movie', 'Perform', 'hoailinh.png'),
(6, 'hongocha@gmail.com', '1234567', 'Ha', 'Ho', 'Model', 'VietNam', 'Movie, Model, Sing', 'Model,Singer', 'hongocha.png'),
(7, 'khanhthi@gmail.com', '12345nbgf', 'Thi', 'Khanh', 'Dancer', 'VietNam', 'shopping,dance', 'Dancer', 'khanhthi.png'),
(8, 'nadal@gmail.com', '12345nbgf', 'Nadal', 'Rafael', 'Teniser', 'Spain', 'tennis,soccer', 'Sport', 'nadal.png'),
(9, 'phananh@gmail.com', '12345nbgf', 'Anh', 'Phan', 'MC', 'VietNam', 'sing,shopping', 'MC', 'phananh.png'),
(10, 'ronaldo@gmail.com', '12345nbgf', 'Ronaldo', 'Cristano', 'Footballer', 'Potugal', 'soccer,shopping', 'sport', 'ronaldo.png'),
(11, 'tranthanh@gmail.com', '12345nbgf', 'Thanh', 'Tran', 'Acter', 'VietNam', 'MC,Sing,Perform', 'Acter', 'tranthanh.png'),
(12, 'truonggiang@gmail.com', '12345nbgf', 'Giang', 'Truong', 'Acter', 'VietNam', 'MC,Perform', 'Acter', 'tranthanh.png'),
(13, 'hoquynhhuong@gmail.com', '12345nbgf', 'Huong', 'Ho', 'Singer', 'VietNam', 'Sing,Shopping', 'Singer', 'hoquynhhuong.png');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Trai', 'caongoctrai2711@gmail.com', '$2y$10$E/JBNVTEgOfujPXmbR8ygeaiXEt5QkFfSOguAgQg2N7Gd712PsIlK', 'bo2d6okRWcbUqP7RE3FTKvDHCWtkaATR1kLlobbkhoj9M7V65GNrMhIsZ966', '2017-07-10 21:55:59', '2017-07-10 21:55:59'),
(2, 'quynh', 'quynhhi@gmail.com', '$2y$10$vBoBs5mT.6/jZR/9vGDbS.yE8ClLbsOBKoCfbfuWLG6HdnsxUK20C', 'g00YtFt5s9579qO2kn5M7PWOkpqvT9ckcT4kQxx6kt1MwBdJmsHvLj4A6wsj', '2017-07-10 21:56:28', '2017-07-10 21:56:28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
